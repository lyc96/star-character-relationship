# import networkx as nx
# import matplotlib.pyplot as plt
# # 支持中文
# plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
# plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
#
# # 有向图网络
# G2 = nx.DiGraph()
# G2.add_edge('李易峰', '郭艾伦')
# G2.add_edge('李易峰', '朱一龙')
# G2.add_edge('朱一龙', '彭冠英')
# G2.add_edge('朱一龙', '孙俪')
# G2.add_edge('李易峰', '唐嫣')
# G2.add_edge('李易峰', '李泌')
# G2.add_edge('李易峰', '杨幂')
#
# nx.spring_layout(G2)
# nx.draw_networkx(G2)
# plt.show()

# -*- coding: utf-8 -*-

import requests
import time
from lxml import etree
import os
import json


headers = {
            'user-agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3947.100 Safari/537.36',
}

name_i = "李易峰"

url_name = "https://baike.baidu.com/search/word?word="+str(name_i)
s = requests.Session()
response = s.get(url_name, headers=headers)
text = response.text
t_split = text.split('id="J-vars" data-lemmaid="')[1].split('" data-lemmatitle="')[0]

url="https://baike.baidu.com/item/"+str(name_i)+"/"+str(t_split)+"?fr=aladdin"
res = requests.get(url,headers=headers)
res.encoding = 'utf-8'
text = res.text
selector = etree.HTML(text)


# #属性
# key = []
# #值
# value = []
#
# dt =[]
# dd =[]
relations = selector.xpath('//*[@id="slider_relations"]/ul/li')
for i in relations:
    re = i.xpath('.//div[@class="name"]/text()')[0]
    name = i.xpath('.//div[@class="name"]/em/text()')[0]
    print(re+"-"+name)

print(len(relations))
# dt.append(basicInfo_left.xpath('.//dt'))
# dd.append(basicInfo_left.xpath('.//dd'))
#
# basicInfo_right = selector.xpath('//*[@class="basicInfo-block basicInfo-right"]')[0]
# dt.append(basicInfo_right.xpath('.//dt'))
# dd.append(basicInfo_right.xpath('.//dd'))
#
#
# for j in dt:
#     for i in j:
#         text = i.xpath('.//text()')
#         if len(text)==1:
#             text = text[0].replace(" ","").replace("\n","").replace("\xa0","")
#         else:
#             text = "-".join(text)
#             text = text.replace(" ", "").replace("\n", "").replace("\xa0", "")
#         key.append(text)
# for j in dd:
#     for i in j:
#         text = i.xpath('.//text()')
#         if len(text) == 1:
#             text = text[0].replace(" ", "").replace("\n", "").replace("\xa0", "")
#         else:
#             text = "-".join(text)
#             text = text.replace(" ", "").replace("\n", "").replace("\xa0", "").replace("-", " ")
#         value.append(text)
#
# links=[]
#
# for k in range(0,len(key)):
#     if key[k]=="代表作品"  or key[k]=="主要成就":
#         v = value[k].split(" ")
#         dict = {'source': str(name_i), 'target': str(v[0]+v[1]), 'rela': str(key[k]), 'type': 'resolved'}
#         links.append(dict)
#     else:
#         dict= {'source': str(name_i), 'target': str(value[k]), 'rela': str(key[k]), 'type': 'resolved'}
#         links.append(dict)

